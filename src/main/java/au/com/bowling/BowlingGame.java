package au.com.bowling;

import au.com.bowling.exception.BowlingException;

/**
 * Created by Rohitha Wanni Achchige on 27/1/17.
 * Represents bowling play game
 */
public class BowlingGame {
    private int currentFrame = 0;
    private boolean isFirstAttemptInFrame = true;
    private BowlingScore bowlingScore;

    public BowlingGame(BowlingScore bowlingScore) {
        this.bowlingScore = bowlingScore;
    }

    /**
     * Calculate score
     *
     * @return int - Total score
     */
    public int score() throws BowlingException {
        return bowlingScore.scoreForFrame(currentFrame);
    }

    /**
     * Play game with provided string
     *
     * @param pins string which knocked over the game
     */
    public void play(String pins) throws BowlingException {
        if (pins != null && !pins.isEmpty()) {
            String[] attempts = pins.split(" ");
            if (attempts.length > Constants.MAX_NO_OF_ATTEMPTS.getID()) {
                throw new BowlingException("Invalid number of attempts provided");
            }
            for (String attempt : attempts) {
                try {
                    int balls = Integer.valueOf(attempt);
                    if (balls >= 0 && balls <= 10) {
                        addAttempt(balls);
                    } else {
                        throw new BowlingException("Invalid rolling pins provided. Rolling pins must be between 0 and 10");
                    }
                } catch (NumberFormatException e) {
                    throw new BowlingException("Invalid rolling pins provided");
                }
            }
            // If a final score cannot be determined from the input then method should return the "current" score
            // (i.e. assumes any remaining bowls scored 0)
            if (attempts.length < Constants.MAX_NO_OF_ATTEMPTS.getID()) {
                addAttempt(0);
            }
        } else {
            throw new BowlingException("Invalid string provided");
        }
    }

    /**
     * Keeps track of pins knocked over
     *
     * @param noOfPins knocked over per for attempt
     */
    protected void addAttempt(int noOfPins) {
        bowlingScore.addAttempt(noOfPins);
        adjustCurrentFrame(noOfPins);
    }

    /**
     * Adjust the frame based on pins
     *
     * @param pins knocked over per for attempt
     */
    private void adjustCurrentFrame(int pins) {
        if (isFirstAttemptInFrame) {
            if (!adjustFrameForStrike(pins))
                isFirstAttemptInFrame = false;
        } else {
            isFirstAttemptInFrame = true;
            advanceFrame();
        }
    }

    protected boolean adjustFrameForStrike(int pins) {
        if (pins == 10) {
            advanceFrame();
            return true;
        }
        return false;
    }

    private void advanceFrame() {
        currentFrame = Math.min(10, currentFrame + 1);
    }
}
