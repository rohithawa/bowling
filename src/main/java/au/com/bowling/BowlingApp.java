package au.com.bowling;

import au.com.bowling.exception.BowlingException;

/**
 * Created by Rohitha Wanni Achchige on 27/1/17.
 * Represents bowling application
 */
public class BowlingApp {
    public static void main(String[] args) {
        BowlingGame bowlingGame = new BowlingGame(new BowlingScore());
        try {
            bowlingGame.play(args[0]);
            System.out.println("Total score: " + bowlingGame.score());
        } catch (BowlingException e) {
            System.out.println(e.getMessage());
        }
    }
}
