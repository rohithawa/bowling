package au.com.bowling.exception;

/**
 * Created by Rohitha Wanni Achchige on 28/1/17.
 * Represents an exception for the bowling game
 */
public class BowlingException extends Exception{
    public BowlingException(String message) {
        super(message);
    }
}
