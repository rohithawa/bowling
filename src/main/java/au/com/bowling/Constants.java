package au.com.bowling;

/**
 * Created by Rohitha Wanni Achchige on 27/1/17.
 * Constants
 */
public enum Constants {
    MAX_NO_OF_ATTEMPTS(21);

    private int id;

    Constants(int id){
        this.id = id;
    }

    public int getID(){
        return id;
    }
}
