package au.com.bowling;

import au.com.bowling.exception.BowlingException;

/**
 * Created by Rohitha Wanni Achchige on 27/1/17.
 * Calculate score based on given attempts
 */
public class BowlingScore {
    private int ball;
    private int[] attempts = new int[Constants.MAX_NO_OF_ATTEMPTS.getID()];
    private int currentAttempts = 0;

    /**
     * Add attempt
     *
     * @param pins - no of pins
     */
    public void addAttempt(int pins) {
        attempts[currentAttempts++] = pins;
    }

    /**
     * Calculate score until provided frame
     *
     * @param frame - frame number to calculate the score
     * @return int - Total score
     */
    public int scoreForFrame(int frame) throws BowlingException {
        ball = 0;
        int score = 0;
        for (int frameCounter = 0; frameCounter < frame; frameCounter++) {
            if (isStrike()) {
                score += 10 + calculateScoreForNextFrame();
            } else if (isSpare()) {
                score += 10 + calculateScoreForNextAttempt();
            } else {
                score += calculateScoreForCurrentFrame();
            }
        }
        return score;
    }

    /**
     * Check whether attempt is Strike
     *
     * @return boolean - Is Strike or not
     */
    protected boolean isStrike() {
        if (attempts[ball] == 10) {
            ball++;
            return true;
        }
        return false;
    }

    /**
     * Check whether attempt is Spare
     *
     * @return boolean - Is Spare or not
     */
    protected boolean isSpare() throws BowlingException {
        if ((attempts[ball] + attempts[ball + 1]) == 10) {
            ball += 2;
            return true;
        } else if ((attempts[ball] + attempts[ball + 1]) > 10) {
            throw new BowlingException("Invalid total pins per frame");
        }
        return false;
    }

    /**
     * Calculate score for next frame (two attempts)
     *
     * @return int - score for next two attempts
     */
    protected int calculateScoreForNextFrame() {
        return attempts[ball] + attempts[ball + 1];
    }

    /**
     * Calculate score for next attempt
     *
     * @return int - score for next attempt
     */
    protected int calculateScoreForNextAttempt() {
        return attempts[ball];
    }

    /**
     * Calculate score for current frame
     *
     * @return int - score for current frame
     */
    protected int calculateScoreForCurrentFrame() {
        return attempts[ball++] + attempts[ball++];
    }
}
