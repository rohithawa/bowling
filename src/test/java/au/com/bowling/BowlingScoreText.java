package au.com.bowling;

import au.com.bowling.exception.BowlingException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Rohitha Wanni Achchige on 29/1/17.
 * BowlingScoreText
 */
public class BowlingScoreText {
    private BowlingScore bowlingScore;

    @Before
    public void init()
    {
        bowlingScore = new BowlingScore();
    }

    @Test
    public void testIsStrike() throws BowlingException
    {
        bowlingScore.addAttempt(10);
        Assert.assertTrue(bowlingScore.isStrike());
    }

    @Test
    public void testIsNotStrike() throws BowlingException
    {
        bowlingScore.addAttempt(9);
        Assert.assertFalse(bowlingScore.isStrike());
    }

    @Test
    public void testIsSpare() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(4);
        Assert.assertTrue(bowlingScore.isSpare());
    }

    @Test
    public void testIsNotSpare() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(3);
        Assert.assertFalse(bowlingScore.isSpare());
    }

    @Test
    public void testScoreForFrame() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(3);
        Assert.assertEquals(9, bowlingScore.scoreForFrame(1));
    }

    @Test
    public void testScoreForFrameForSpare() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(4);
        Assert.assertEquals(10, bowlingScore.scoreForFrame(1));
    }

    @Test
    public void testScoreForFrameForStrike() throws BowlingException
    {
        bowlingScore.addAttempt(10);
        bowlingScore.addAttempt(4);
        bowlingScore.addAttempt(2);
        Assert.assertEquals(22, bowlingScore.scoreForFrame(2));
    }

    @Test
    public void testCalculateScoreForNextFrame() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(4);
        Assert.assertEquals(10, bowlingScore.calculateScoreForNextFrame());
    }

    @Test
    public void testCalculateScoreForNextAttempt() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(4);
        Assert.assertEquals(6, bowlingScore.calculateScoreForNextAttempt());
    }

    @Test
    public void testCalculateScoreForCurrentFrame() throws BowlingException
    {
        bowlingScore.addAttempt(6);
        bowlingScore.addAttempt(4);
        Assert.assertEquals(10, bowlingScore.calculateScoreForCurrentFrame());
    }
}
