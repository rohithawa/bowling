package au.com.bowling;

import au.com.bowling.exception.BowlingException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Rohitha Wanni Achchige on 27/1/17.
 * BowlingGameTest
 */

public class BowlingGameTest
{
    private BowlingGame bowlingGame;

    @Before
    public void init()
    {
        bowlingGame = new BowlingGame(new BowlingScore());
    }

    @Test
    public void testFourAttempts() throws BowlingException
    {
        bowlingGame.play("1 2 3 4");
        Assert.assertEquals(10, bowlingGame.score());
    }

    @Test
    public void testFourAttemptsWithTwoSpares() throws BowlingException
    {
        bowlingGame.play("9 1 9 1");
        Assert.assertEquals(29, bowlingGame.score());
    }

    @Test
    public void testSevenAttemptsWithOneStrike() throws BowlingException
    {
        bowlingGame.play("1 1 1 1 10 1 1");
        Assert.assertEquals(18, bowlingGame.score());
    }

    @Test
    public void testTenStrikes() throws BowlingException
    {
        bowlingGame.play("10 10 10 10 10 10 10 10 10 10 10 10");
        Assert.assertEquals(300, bowlingGame.score());
    }

    @Test
    public void testMaximumAttempts() throws BowlingException
    {
        bowlingGame.play("9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1 9");
        Assert.assertEquals(190, bowlingGame.score());
    }

    @Test
    public void testIncompletedLastAttempt() throws BowlingException
    {
        bowlingGame.play("9 0 9");
        Assert.assertEquals(18, bowlingGame.score());
    }

    @Test(expected = BowlingException.class)
    public void testEmptyString() throws BowlingException
    {
        bowlingGame.play("");
    }

    @Test(expected = BowlingException.class)
    public void testInvalidRollingNumber() throws BowlingException
    {
        bowlingGame.play("1 11");
    }

    @Test(expected = BowlingException.class)
    public void testInvalidRollingAttempts() throws BowlingException
    {
        bowlingGame.play("9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1 9 1");
    }

    @Test
    public void testZero() throws BowlingException
    {
        bowlingGame.play("0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0");
        Assert.assertEquals(0, bowlingGame.score());
    }

    @Test
    public void testAllOnes() throws BowlingException
    {
        bowlingGame.play("1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1");
        Assert.assertEquals(20, bowlingGame.score());
    }

    @Test
    public void testOneSpare() throws BowlingException
    {
        bowlingGame.play("5 5 3");
        Assert.assertEquals(16, bowlingGame.score());
    }

    @Test
    public void testOneStrike() throws BowlingException
    {
        bowlingGame.play("10 5 3");
        Assert.assertEquals(26, bowlingGame.score());
    }

    @Test
    public void testAddAttempt() throws BowlingException
    {
        bowlingGame.addAttempt(10);
        bowlingGame.addAttempt(5);
        bowlingGame.addAttempt(3);
        Assert.assertEquals(26, bowlingGame.score());
    }

    @Test
    public void testAdjustFrameForStrike() throws BowlingException
    {
        Assert.assertTrue(bowlingGame.adjustFrameForStrike(10));
    }

    @Test
    public void testAdjustFrameForNotStrike() throws BowlingException
    {
        Assert.assertFalse(bowlingGame.adjustFrameForStrike(9));
    }
}
